$(document).ready(function(e) {

  var mySwiper = new Swiper ('.js_slideWrap', {
	direction: 'horizontal', // 垂直切换选项
	loop: true, // 循环模式选项
	autoplay: {
		delay: 5000
	},//可选选项，自动滑动
	
	// 如果需要分页器
	pagination: {
	  el: '.swiper-pagination'
	},
	
	// 如果需要前进后退按钮
	navigation: {
	  nextEl: '.swiper-button-next',
	  prevEl: '.swiper-button-prev',
	}
  })        

})