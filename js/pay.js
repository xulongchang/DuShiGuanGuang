var ns = {
	bindEvt:function(){
		var self = this;
		$(".js-pay").on("click",function(){
			self.showPayTip();
			window.open("https://www.alipay.com/");//todo这里需要打开支付页面
		})
		$(".js-cancel").on("click",function(){
			layer.closeAll() ;
		})
	},
	showPayTip:function(){
		this.layWin = layer.open({
		  type: 1,
		  closeBtn: 0,
		  title: false, //不显示标题
		  area:["600px","200px"],
		  content: $('.layer_notice'), //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
		  cancel: function(){
			
		  }
		});
	},
	hideWxpPay:function(){
		$(".wxPay").addClass("hide");
		$(".aliyPay").removeClass("hide");
	},
	showWxPay:function(){
		$(".wxPay").removeClass("hide");
		$(".aliyPay").addClass("hide");
	},
	payType:0,//默认支付宝
	changePay:function(){
		var self = this;
		$('[name="paytype"]').on("change",function(){
			var val = $('[name="paytype"]:checked').val();
			if(val == 0){//支付宝
				//window.open("https://www.alipay.com/");
				self.hideWxpPay();
			}else{//微信
				self.showWxPay();
			}
			$(".pay-cell").eq(val).addClass("on").siblings().removeClass("on");
		})
	},
	init:function(){
		this.changePay();
		this.bindEvt();
	}
}

$(document).ready(function(){
	ns.init();
})