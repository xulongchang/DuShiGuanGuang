var ns = {
	bindEvt:function(){
		//加入购物车
		$(".js-addcart").on("click",function(){
			var id = $(this).attr("data-id");
			//发送ajax加入购物车
			//加入成功后给出提示
			layer.msg('已加入购物车', {time: 5000, icon:6});
		})
	},
	//详情点击定位跳转
	navClick:function(){
		$("#navigate-list li").on("click",function(){
			var index = $(this).index();
			var $cell = $(".js-content-detail .cell").eq(index);
			$("html").scrollTo( $cell, 100 ,{offset:{top:-43}});
		})
		$(".js-gobuy").on("click",function(){
			$("html").scrollTo( $("#js-list-piaozhong"), 100 ,{offset:{top:-43}});
		})
	},
	//滚动做定位
	scrollFn:function(){
		$(window).on("scroll",function(){
			var ScrollTop = $(this).scrollTop();
			$(".js-content-detail .cell").each(function(){
				var $this = $(this);
				var clientRect = $this[0].getBoundingClientRect();
				var index = $this.index();
				console.log()
				var cTop = clientRect.top;
				cTop = cTop - 50;
				if(cTop <=0){
					$("#navigate-list li a").eq(index).addClass("active")
					$("#navigate-list li").eq(index).siblings().find("a").removeClass("active");
				}
				
			})
			
			var $productdetailheader = $("#product-detail-header");
			if($productdetailheader[0].getBoundingClientRect().top<=0){
				$productdetailheader.addClass("isfixed");
			}else{
				$productdetailheader.removeClass("isfixed");
			}
		})
	},
	init:function(){
		this.navClick();
		this.scrollFn();
		this.bindEvt();
	}
}

$(document).ready(function(){
	ns.init();
	
	$(function(){
		// 无缝滚动开始
		prevAllow = true,    //为了防止连续点击上一页按钮
		nextAllow = true;    //为了防止连续点击下一页按钮
		
		$(".pre").click(function(){	
		if (prevAllow) {prevAllow = false;
		var fstatc=$(this).parent().find(".Atcin_ul li:eq(0)");	
		$(this).parent().find(".Atcin_ul li:last").clone().insertBefore(fstatc);
		fstatc.prev().bind("click",function(){
		var aa=$(this).attr("id");
		$(this).siblings().removeClass().end().addClass("on");				
		$("."+aa).siblings().hide().end().show();
		});	
		$(this).parent().find(".Atcin").css("left","-175px");
		$(this).parent().find(".Atcin").animate({left:"+=175px"},500,function(){		
		$(this).parent().find(".Atcin_ul li:last").remove();	
		prevAllow = true;
			}); 
			$(".cj1").animate({"background-position":"+=215px"},500);
			$(".cj2").animate({"background-position":"+=355px"},500);
			$(".car").removeClass().addClass("car2");
			return false;
		}
		});	
		$(".next").click(function(){
		if (nextAllow) {nextAllow = false;	
		var lastatc=$(this).parent().find(".Atcin_ul li:last")	
		$(this).parent().find(".Atcin_ul li:eq(0)").clone().insertAfter(lastatc);    
		lastatc.next().bind("click",function(){
		var aa=$(this).attr("id");
		$(this).siblings().removeClass().end().addClass("on");				
		$("."+aa).siblings().hide().end().show();
		});	
		$(this).parent().find(".Atcin").animate({left:"-175px"}, 500,function(){
			$(this).parent().find(".Atcin_ul li:eq(0)").remove();
			$(this).parent().find(".Atcin").css("left","0px");
			nextAllow = true;
			});
			$(".cj1").animate({"background-position":"-=215px"},500);
			$(".cj2").animate({"background-position":"-=355px"},500);
			$(".car2").removeClass().addClass("car");
			return false;
		}
		});
		// 无缝滚动结束
		// 景点切换开始
		$(".c1").show();$(".d1").show();$(".f1").show();
		$(".Atcin_ul li").click(function () {
			var aa=$(this).attr("id");
			$(this).siblings().removeClass().end().addClass("on");
			$("."+aa).siblings().hide().end().show();
			});
		// 景点切换结束
		// 线路切换开始
		$(".Atc1").show();
		$(".line_tab li").click(function () {
			var bb = $(this).attr("id");
			$(this).siblings("li:not(#Atc4)").removeClass().end().addClass("on");				
			$("."+bb).show().siblings().not(".line_tab").hide();
			//$("."+bb).siblings("div:not(#Atc4)").not(".line_tab").hide().end().show();
		});	
		// 线路切换结束
		
		if ( $.browser.msie && $.browser.version<7){
		$(".ie6").addClass("ie6s");
		}
		
		$(".Atcin_ul").eq(0).find("li").eq(0).click();
		
	});
})