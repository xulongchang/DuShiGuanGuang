$(document).ready(function(e) {
	/*页面加载计算价格*/
	subprice();
	
	/*加减数量计算价格*/
	$(".js_num_reduce , .js_num_plus").click(function(e) {
        subprice();
    });
	
	/*改变数量时*/
	$(".js_num_text").keyup(function(e) {
		
        subprice();
    });
});

/*计算小计*/
function subprice(){
	
	/*总计初始化*/
	var totalNum = 0;
	
	/*遍历计算小计,顺便计算出总价*/
	$("#js_cart_list").children("tr").each(function(index, element) {
        var price = parseFloat($(this).find(".js_price").attr("data-price"));
		var num = parseFloat($(this).find(".js_num_text").val());
		var subprice = price * num;
		$(this).find(".js_xiaojinum").text(subprice);
		
		/*计算总价*/
		totalNum += subprice;
		$("#js_totalprice").text(totalNum);
    });
	
}